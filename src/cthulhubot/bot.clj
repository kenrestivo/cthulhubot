(ns cthulhubot.bot
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [cthulhubot.cthulhu :as cthulhu]
            [matrix-clj.matrix :as matrix]
            [matrix-clj.matrix.net :as net]
            [matrix-clj.matrix.streams :as streams]
            [clj-http.util :as hutil]
            [cheshire.core :as json]
            [clj-http.client :as client]))



(defn process-stream!
  "Takes a token, a matrix base url, and a stream.
   Processes each event on the stream, and does side-effecting things like spin off replies."
  [token base-url stream]
  (log/trace "processing stream" base-url)
  (doseq [room (streams/stream->invites stream)]
    (future (ulog/catcher
             (net/join-room! token base-url room))))
  ;; TODO: make this configurable?  maybe cthulhu is taken on this homeserver?
  (doseq [room (streams/stream->mentions "cthulhu" stream)]
    (future (ulog/catcher
             (log/debug "sending message to" room)
             (net/send-message! token base-url room (cthulhu/exclamation (+ 5 (rand-int 25))))))))


