(ns cthulhubot.core
  (:gen-class)
  (:require [cthulhubot.bot :as bot]
            [matrix-clj.matrix :as matrix]
            [cthulhubot.config :as config]))

(defn -main
  [config-file]
  (let [{:keys [base-url timeout creds]} (config/read-from-file config-file)
        {:keys [access_token]} creds]
    (matrix/run-sync access_token
                  base-url
                  timeout
                  bot/process-stream!)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment



  )
