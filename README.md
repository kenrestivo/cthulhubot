# cthulhubot

A bot that brings eldritch horrors to your Matrix channels.

Via TEttinger via  https://gist.github.com/stathissideris/f1bced0dfdebdaf488eb 

## Installation

Build with lein uberjar. Resulting uberjar will be in "target/" directory

Or you can run it with lein run too.

## Running

    $ java -jar cthulhu.jar config-file.edn


## Examples

If you want to try the bot out, it's currently online and addressable at @cthulhu:spaz.org. Just invite it to a chat (private or public, it doesn't care) and mention its name in the chat. It will respond with an exclamation.

## License

Copyright © 2018 ken restivo <ken@restivo.org> and 2015 TEttinger

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
