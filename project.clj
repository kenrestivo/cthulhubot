(defproject cthulhubot "0.1.0"
  :description "Bring eldritch horrors to your Matrix chats"
  :url "https://gitlab.com/kenrestivo/cthulhubot"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [utilza "0.1.99"]
                 [com.taoensso/timbre "4.10.0"]
                 [matrix-clj "0.1.0"]
                 [org.clojure/tools.trace "0.7.9"]]
  :main ^:skip-aot cthulhubot.core
  :target-path "target/%s"

  :profiles {:uberjar {:uberjar-name "cthulhu.jar"
                       :source-paths ["src"]
                       :aot :all}})
